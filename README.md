![alt tag](http://robertochoa.com.ve/repos/harleystreet.jpg)

# HarleyStreet Healthcare Clinic Wordpress Custom Theme #

* Version: 1.0
* Design: [Nextdots](http://www.nextdots.com/)
* Development: [Robert Ochoa](http://www.robertochoa.com.ve/)


### Main Components ###

* Twitter Bootstrap 3.3.7
* Animate 3.5.1
* Desandro's Flickity 2.0.2
* OwlCarousel 1.3.3
* Font Awesome 4.6.8
* jQuery Nicescroll 3.6.8
* modernizr 2.8.3
* jQuery Sticky 1.0.4
* Lettering 0.7.0
* Smooth Scroll 10.0.0
* Desandro's Isotope 3.0.1
* Desandro's Masonry 4.1.0

### Included Functions ###

* Custom Post Type:
* Custom Taxonomies:
* Wordpress Menu Structure
* Custom Metabox:
* Custom contact section.

### Required Plugins ###

* Rilwis' Metabox
* Jetpack by Wordpress
