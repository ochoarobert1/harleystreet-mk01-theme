
<?php /* POST FORMAT - DEFAULT */ ?>

<article id="post-<?php the_ID(); ?>" class="the-single col-md-9 <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">
    <?php if ( has_post_thumbnail()) : ?>
    <picture>
        <?php the_post_thumbnail('single_img', $defaultargs); ?>
    </picture>
    <?php endif; ?>
    <header>
    
        <h1 itemprop="name"><?php the_title(); ?></h1>
        <?php the_tags( __( 'Tags: ', 'harleystreet' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>
    </header>
    <div class="post-content" itemprop="articleBody">
        <?php the_content() ?>
       
    </div><!-- .post-content -->
    <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
    <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
    <meta itemprop="url" content="<?php the_permalink() ?>">
    
</article> <?php // end article ?>
