<?php get_header(); ?>
<?php the_post(); ?>
<main class="container">
    <div class="row">
        <div class="single-main-container single-service-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
            <?php $defaultargs = array('class' => 'img-responsive'); ?>
            <article id="post-<?php the_ID(); ?>" class="the-single col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">
                <header>
                    <div class="single-service-title-img">
                        <?php $images = rwmb_meta( 'rw_service_logo', 'size=avatar' ); ?>
                        <?php if ( !empty( $images ) ) { foreach ( $images as $image ) { ?>
                        <?php echo "<img src='{$image['url']}' class='img-responsive' width='{$image['width']}' height='{$image['height']}' alt='{$image['alt']}' />"; ?>
                        <?php } } ?>
                    </div>
                    <h1 itemprop="name"><?php the_title(); ?></h1>
                </header>
                <div class="post-content col-lg-12 col-md-12 col-sm-12 col-xs-12" itemprop="articleBody">
                    <?php if ( has_post_thumbnail()) : ?>
                    <picture>
                        <?php the_post_thumbnail('single_img', $defaultargs); ?>
                    </picture>
                    <?php endif; ?>
                    <?php the_content() ?>

                </div><!-- .post-content -->
                <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                <meta itemprop="url" content="<?php the_permalink() ?>">

            </article> <?php // end article ?>
        </div>
    </div>
</main>
<?php get_footer(); ?>
