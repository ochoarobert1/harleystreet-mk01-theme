<form role="search" method="get" id="searchform" class="form-shop-custom searchform input" action="<?php echo home_url( '/' ); ?>">
    <div class="input-group">
        <span class="input-group-btn">
            <button type="submit" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
        </span>
        <input type="hidden" name="post_type[]" value="services" />
        <input type="hidden" name="post_type[]" value="page" />
        <input id="woocommerce-product-search-field" type="text" class="form-control" placeholder="<?php _e( 'Search for terms:', 'harleystreet' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php _e( 'Search for terms:', 'harleystreet' ); ?>" >
    </div><!-- /input-group -->
</form>
