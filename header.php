<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <?php /* MAIN STUFF */ ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
        <meta name="robots" content="NOODP, INDEX, FOLLOW" />
        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="dns-prefetch" href="//connect.facebook.net" />
        <link rel="dns-prefetch" href="//facebook.com" />
        <link rel="dns-prefetch" href="//googleads.g.doubleclick.net" />
        <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" />
        <link rel="dns-prefetch" href="//google-analytics.com" />
        <?php /* FAVICONS */ ?>
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
        <?php /* THEME NAVBAR COLOR */ ?>
        <meta name="msapplication-TileColor" content="#5c9f6e" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
        <meta name="theme-color" content="#5c9f6e" />
        <?php /* AUTHOR INFORMATION */ ?>
        <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
        <meta name="author" content="NextDots" />
        <meta name="copyright" content="http://www.harleystreet104.com" />
        <meta name="geo.position" content="51.5213031,-0.1485044" />
        <meta name="ICBM" content="51.5213031,-0.1485044" />
        <meta name="geo.region" content="UK" />
        <meta name="geo.placename" content="104 Harley Street, London W1G 7JD" />
        <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
        <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
        <?php wp_title('|', false, 'right'); ?>
        <?php wp_head() ?>
        <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
        <?php get_template_part('includes/header-oginfo'); ?>
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php /* IE COMPATIBILITIES */ ?>
        <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
        <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
        <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
        <!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js" /><!--<![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
        <?php get_template_part('includes/fb-script'); ?>
        <?php get_template_part('includes/ga-script'); ?>
    </head>
    <body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
        <div id="fb-root"></div>
        <?php if (is_page('cart')) { ?>
        <div class="special-button-mobile special-button-mobile-cart hidden-lg hidden-md hidden-sm col-xs-12 no-paddingl no-paddingr">
            <?php global $woocommerce; ?>
            <?php  $amount = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_total() ) ); ?>
            <div class="special-button-price col-xs-2">
                <?php echo get_woocommerce_currency_symbol() . ' ' . $amount; ?>
            </div>
            <div class="special-button-button col-xs-10">
                <a href="<?php echo $woocommerce->cart->get_checkout_url(); ?>" class="btn btn-md btn-special-button">
                    Proceed to Checkout
                </a>
            </div>
        </div>
        <?php } else { ?>
        <?php if (is_product()) { ?>
        <div class="special-button-mobile special-button-mobile-cart hidden-lg hidden-md hidden-sm col-xs-12 no-paddingl no-paddingr">
            <?php global $woocommerce; ?>
            <?php  $amount = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_total() ) ); ?>
            <div class="special-button-price col-xs-2">
                <?php echo get_woocommerce_currency_symbol() . ' ' . $amount; ?>
            </div>
            <div class="special-button-button col-xs-10">
                <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" class="btn btn-md btn-special-button">
                    go to My Cart
                </a>
            </div>
        </div>
        <?php } else { ?>
        <div class="special-button-mobile hidden-lg hidden-md hidden-sm col-xs-12">
            <a href="https://www.astutedatasystems.com/home.aspx?ID=780" class="btn btn-md btn-special-button">
                <i class="fa fa-calendar-check-o"></i> Book an Appointment 
            </a>
        </div>
        <?php } ?>
        <?php } ?>
        <header id="sticker" class="container-fluid" role="banner" itemscope itemtype="http://schema.org/WPHeader">
            <div class="row">
                <div class="search-header search-header-hide col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr hidden-xs">
                    <div class="container">
                        <div class="row">
                            <div class="search-header-container col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                <?php get_search_form(); ?>
                            </div>
                            <div class="search-closer-container col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                <i id="search-closer" class="fa fa-close"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pre-header col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr hidden-xs">
                    <div class="container">
                        <div class="row">
                            <div class="pre-header-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <i class="fa fa-phone"></i> <a href="tel:+4402079356554" title="<?php _e('Call us', 'harleystreet'); ?>"><span> 020 7935 6554</span></a>
                                <i class="fa fa-envelope-o"></i> <a href="mailto:info@harleystreet104.com" title="<?php _e('Send us an e-mail', 'harleystreet'); ?>"><span>info@harleystreet104.com</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="the-header col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="container">
                        <div class="row">
                            <div class="the-navbar col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-xs">
                                <nav class="navbar navbar-default" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                                    <div class="container-fluid">
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" itemscope itemtype="http://schema.org/Organization"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="HarleyStreet" class="img-responsive img-logo"></a>
                                        </div>

                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                            <ul class="nav navbar-nav navbar-right navbar-custom">
                                                <li><a id="search-opener"><i class="fa fa-search"></i></a></li>
                                                <li><a href="<?php echo home_url('/cart'); ?>"><i class="fa fa-shopping-cart"></i></a></li>
                                                <li><a type="button" href="<?php echo home_url('/book-an-appointment'); ?>" class="btn btn-default navbar-btn">Book an Appointment</a></li>

                                            </ul>

                                            <?php wp_nav_menu(array('container_class' => 'menu-header', 'theme_location' => 'header_menu', 'items_wrap' => '<ul id="%1$s" class="%2$s nav navbar-nav navbar-right">%3$s</ul>', 'walker' => new BS3_Walker_Nav_Menu )); ?>


                                        </div><!-- /.navbar-collapse -->
                                    </div><!-- /.container-fluid -->
                                </nav>
                            </div>
                            <div  class="the-navbar-mobile  hidden-lg hidden-md hidden-sm col-xs-12">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <button id="navbar-mobile-opener" type="button" class="navbar-toggle collapsed ">
                                        <span class="icon-bar top-bar"></span>
                                        <span class="icon-bar middle-bar"></span>
                                        <span class="icon-bar bottom-bar"></span>
                                    </button>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-paddingl">
                                    <a href="<?php echo esc_url(home_url('/')); ?>" itemscope itemtype="http://schema.org/Organization"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="HarleyStreet" class="img-responsive "></a>
                                </div>
                                <div class="mobile-cart-container col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <a href="<?php echo home_url('/cart'); ?>"><i class="fa fa-shopping-cart"></i></a>
                                </div>
                                <div id="navbar-mobile" class="the-navbar-mobile-container navbar-mobile-hidden">
                                    <?php wp_nav_menu(array('container_class' => 'menu-header-mobile', 'theme_location' => 'header_menu', 'items_wrap' => '<ul id="%1$s" class="%2$s ">%3$s</ul>' )); ?>
                                    <div class="menu-header-mobile menu-header-mobile-extra">
                                        <ul>
                                            <li><a href="<?php echo home_url('/cart'); ?>"><i class="fa fa-shopping-cart"></i> My Cart</a></li>
                                            <li><a href="https://www.astutedatasystems.com/home.aspx?ID=780"><i class="fa fa-calendar-check-o"></i> Book an Appointment</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
