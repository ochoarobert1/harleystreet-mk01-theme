<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $product, $woocommerce_loop;

if ( empty( $product ) || ! $product->exists() ) {
    return;
}

if ( ! $related = $product->get_related( $posts_per_page ) ) {
    return;
}

$args = apply_filters( 'woocommerce_related_products_args', array(
    'post_type'            => 'product',
    'ignore_sticky_posts'  => 1,
    'no_found_rows'        => 1,
    'posts_per_page'       => $posts_per_page,
    'orderby'              => $orderby,
    'post__in'             => $related,
    'post__not_in'         => array( $product->id )
) );

$products                    = new WP_Query( $args );
$woocommerce_loop['name']    = 'related';
$columns = 1;
$woocommerce_loop['columns'] = apply_filters( 'woocommerce_related_products_columns', $columns );

if ( $products->have_posts() ) : ?>

<div class="related products">

    <h2><?php _e( 'Consumer also consider', 'harleystreet' ); ?></h2>

    <?php woocommerce_product_loop_start(); ?>
    <?php $count = $products->post_count; ?>

    <div class="single-related-carousel col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">

        <?php while ( $products->have_posts() ) : $products->the_post(); ?>
        <article class="home-product-item item <?php if ($count == 1 ) { echo 'home-product-item-singular'; } ?>">
            <picture>
                <a href="<?php the_permalink(); ?>" title="<?php _e('Buy Now', 'harleystreet'); ?>">
                    <?php the_post_thumbnail('product_img', array('class' => 'img-responsive')); ?>
                </a>
            </picture>
            <header>
                <h3><?php the_title(); ?></h3>
            </header>
            <div class="home-product-item-info">
                <p><?php echo $product->get_price_html(); ?></p>
                <a href="<?php the_permalink(); ?>" title="<?php _e('Buy Now', 'harleystreet'); ?>"><?php _e('Buy Now', 'harleystreet'); ?></a>
            </div>
        </article>
        <?php endwhile; // end of the loop. ?>
    </div>

    <?php woocommerce_product_loop_end(); ?>

</div>

<?php endif;

wp_reset_postdata();
