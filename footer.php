<footer class="container-fluid"  role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row">
        <div class="the-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="contact-extra-logo col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/care-quality.png" alt="care quality" class="img-responsive" />
                    </div>
                    <div class="footer-menu col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php wp_nav_menu(array('container_class' => 'menu-footer', 'theme_location' => 'footer_menu', 'items_wrap' => '<ul id="%1$s" class="%2$s navbar-footer-nav">%3$s</ul>' )); ?>
                    </div>
                    <div class="footer-social col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <span><?php _e('FOLLOW US', 'harleystreet'); ?></span>
                            <a href="https://www.facebook.com/harleystreet104/" target="_blank" title="<?php _e('Facebook Page', 'harleystreet'); ?>"><i class="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/harleystreet104" target="_blank" title="<?php _e('Twitter Profile', 'harleystreet'); ?>"><i class="fa fa-twitter"></i></a>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <i class="fa fa-phone"></i> <a href="tel:+4402079356554" title="<?php _e('Call us', 'harleystreet'); ?>"><span> 020 7935 6554</span></a>
                            <i class="fa fa-envelope-o"></i> <a href="mailto:info@harleystreet104.com" title="<?php _e('Send us an e-mail', 'harleystreet'); ?>"><span>info@harleystreet104.com</span></a>
                        </div>
                    </div>
                    <div class="footer-copyright col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p>Copyright 2015 Harley Street Healthcare clinic. All rights reserved.</p>
                        <p>The contects on this site is for information only, and is not meant to substitute the advice or your own phusician or other medical professional.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>

</body>
</html>
