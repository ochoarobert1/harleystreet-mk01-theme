<?php
$contact = get_page_by_path('contacto');
$coord = '51.521332,-0.1480059';
?>

<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyD-FW49_GyRnOVC4any4nf0IU1xyYqzDuY"></script>
<script type="text/javascript">

    var map = null
    var marker = null

    $(document).ready(function() {
        var mapOptions = {
            center: new google.maps.LatLng(<?php echo $coord ?>),
            zoom: 18,   
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            navigationControl: false,
            scaleControl: false,
            optimized: false,
            zIndex:0
            /* HYBRID | ROADMAP | SATELLITE| TERRAIN */
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(<?php echo $coord ?>),
            title: 'título del marcador',
            map: map
            //icon: "<?php echo get_template_directory_uri(); ?>/images/marker.png"
        });

    });
</script>
<div class="clearfix"></div>
<div class="map">
    <div id="map_canvas" class="map_gray" style="width:100%; height:560px;"></div>
</div>
<div class="clearfix"></div>
