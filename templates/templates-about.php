<?php
/**
 * Template Name: About Page
 *
 * @package HarleyStreet
 * @subpackage harleystreet-mk01-theme
 * @since 1.0
 */
?>

<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $images = rwmb_meta( 'rw_page_banner', 'size=full' ); ?>
        <?php if ( !empty( $images ) ) { foreach ( $images as $image ) { ?>
        <?php $img_url = $image['url']; ?>
        <?php } } ?>
        <section class="page-title-container page-about-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" style="background: url(<?php echo $img_url; ?>); background-size: cover; background-position: 0 100px;">
            <div class="container">
                <div class="row">
                    <div class="page-title-content col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <h1 itemprop="headline"><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="container">
                <div class="row">
                    <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
                        <div class="page-article col-lg-12 col-md-12 col-sm-12 col-xs-12" itemprop="articleBody">
                            <?php the_content(); ?>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
