<?php
/**
 * Template Name: Contact Page
 *
 * @package HarleyStreet
 * @subpackage harleystreet-mk01-theme
 * @since 1.0
 */
?>

<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-title-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="page-title-content col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <h1 itemprop="headline"><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="container">
                <div class="row">
                    <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
                        <div class="page-article page-contact-content col-lg-8 col-md-8 col-sm-8 col-xs-12" itemprop="articleBody">
                            <?php the_content(); ?>
                        </div>
                        <picture class="page-contact-picture col-lg-2 col-md-2 col-sm-2 col-md-offset-1 hidden-xs no-paddingl no-paddingr">
                            <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                        </picture>
                    </article>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
