(function() {
    tinymce.PluginManager.add('gavickpro_tc_button', function( editor, url ) {
        editor.addButton( 'gavickpro_tc_button', {
            text: ' Accordion',
            icon: 'icon dashicons-list-view',
            onclick: function() {
                selected = tinyMCE.activeEditor.selection.getContent();

                if( selected ){
                    //If text is selected when button is clicked
                    //Wrap shortcode around it.
                    content =  '[accordion order=" " title=" "]'+selected+'[/accordion]';
                }else{
                    content =  '[accordion order=" " title=" "]';
                }

                tinymce.execCommand('mceInsertContent', false, content);
            }
        });
    });
})();