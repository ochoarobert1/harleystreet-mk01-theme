$(document).ready(function () {
    "use strict";
    jQuery('#servhour').timepicker();
    jQuery.ajax({
        url : servfunctions.ajax_url,
        type : 'post',
        data : {
            action : 'post_servfunctions'
        },
        success : function (response) {
            var obj = jQuery.parseJSON(response);
            jQuery.each(obj, function (i, item) {
                var valNew = item.split(',');
                var texto = valNew[1].replace("&amp;", "\&"); 
                jQuery('#servcontent').append(jQuery('<option>', {
                    value: valNew[0],
                    text : texto
                }));
            });
        }
    });
});