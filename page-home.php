<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="the-slider col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="homepage-hero-module">
                <div class="video-container">
                    <div class="filter">
                        <h2>BOOK NOW</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <a href="<?php echo home_url('/book-an-appointment'); ?>"><button class="btn btn-md btn-book">BOOK AN APPOINTMENT</button></a>
                    </div>
                    <video autoplay loop class="fillWidth">
                        <source src="<?php echo esc_url(get_template_directory_uri()); ?>/video/video.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
                        <source src="<?php echo esc_url(get_template_directory_uri()); ?>/video/video.webm" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
                    </video>
                    <div class="poster hidden">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/slider.png" alt="Video" />
                    </div>
                </div>
            </div>
        </section>
        <section class="the-about col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-lg hidden-md hidden-sm no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="the-about-home col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2><?php _e('About Us', 'harleystreet'); ?></h2>
                        <p>Our clinic is staffed by a qualified multi-disciplinary team of medical practitioners, therapists, technicians and support staff</p>
                        <a href="<?php echo home_url('/about-us'); ?>" title="Get to Know Us"><button class="btn btn-md btn-custom">Get to Know Us</button></a>
                    </div>
                </div>
            </div>
        </section>
        <section class="the-services col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <h2><?php _e('Our Services', 'harleystreet'); ?></h2>
            <?php $args = array('post_type' => 'services',  'post_parent' => 0, 'posts_per_page' => 8, 'order' => 'DESC', 'orderby' => 'date' ); ?>
            <?php query_posts($args); $i = 1; ?>
            <?php if ( have_posts() ) : ?>
            <?php while (have_posts()) : the_post(); ?>
            <?php if ($i == 1) { echo '<div class="home-service-item-content col-lg-12 col-md-12 col-sm-12 col-xs-12">'; } ?>

            <article class="home-service-item col-lg-3 col-md-3 col-sm-3 col-xs-12 animated fadeIn delay-<?php echo $i; ?>">
                <a href="<?php the_permalink(); ?>">
                    <picture>
                        <?php $images = rwmb_meta( 'rw_service_logo', 'size=avatar' ); ?>
                        <?php if ( !empty( $images ) ) { foreach ( $images as $image ) { ?>
                        <?php echo "<img src='{$image['url']}' class='img-responsive' width='{$image['width']}' height='{$image['height']}' alt='{$image['alt']}' />"; ?>
                        <?php } } ?>
                    </picture>
                    <header>
                        <h3><?php the_title(); ?></h3>
                    </header>
                    <div class="home-service-item-info">
                        <p><?php the_excerpt(); ?></p>
                    </div>
                </a>
            </article>

            <?php if ($i == 4) { echo '</div>'; } ?>
            <?php $i++; if ($i > 4) { $i = 1; } ?> 
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </section>

        <section class="the-products col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <h2><?php _e('Our Products', 'harleystreet'); ?></h2>
            <div class="container">
                <div class="row">
                    <div class="home-products-content col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="home-product-carousel col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr owl-carousel owl-theme">
                            <?php $args = array('post_type' => 'product', 'posts_per_page' => 10, 'order' => 'ASC', 'orderby' => 'date', 'meta_query'  => array( array( 'key' => '_featured', 'value' => 'yes') ) ); ?>
                            <?php query_posts($args); ?>
                            <?php if ( have_posts() ) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <?php $product = new WC_Product( get_the_ID() ); ?>
                            <article class="home-product-item item">
                                <picture>
                                    <?php the_post_thumbnail('product_img', array('class' => 'img-responsive')); ?>
                                </picture>
                                <header>
                                    <h3><?php the_title(); ?></h3>
                                </header>
                                <div class="home-product-item-info">
                                    <p><?php echo $product->get_price_html(); ?></p>
                                    <a href="<?php the_permalink(); ?>" title="<?php _e('Buy Now', 'harleystreet'); ?>"><?php _e('Buy Now', 'harleystreet'); ?></a>
                                </div>
                            </article>
                            <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                        <a href="<?php echo home_url('/our-products'); ?>" title="<?php _e('View All Products', 'harleystreet'); ?>" class="home-products-button"><?php _e('View All Products', 'harleystreet'); ?></a>
                    </div>
                </div>
            </div>
        </section>

        <section class="the-partners col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <h2><?php _e('Partners', 'harleystreet'); ?></h2>
            <div class="container">
                <div class="row">
                    <div class="home-partners-content col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                        <div class="home-partners-carousel col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr owl-carousel owl-theme">
                            <?php $args = array('post_type' => 'partners', 'posts_per_page' => 10, 'order' => 'ASC', 'orderby' => 'date' ); ?>
                            <?php query_posts($args); ?>
                            <?php if ( have_posts() ) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <?php $product = new WC_Product( get_the_ID() ); ?>
                            <article class="home-partners-item item">
                                <picture>
                                    <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                                </picture>
                            </article>
                            <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="the-testimonials col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <h2><?php _e('Testimonials', 'harleystreet'); ?></h2>
            <div class="home-testimonials-content col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
                            <div class="home-testimonials-carousel col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr owl-carousel owl-theme">
                                <?php $args = array('post_type' => 'testimonials', 'posts_per_page' => 10, 'order' => 'ASC', 'orderby' => 'date' ); ?>
                                <?php query_posts($args); ?>
                                <?php if ( have_posts() ) : ?>
                                <?php while (have_posts()) : the_post(); ?>
                                <?php $product = new WC_Product( get_the_ID() ); ?>
                                <article class="home-testimonials-item item">
                                    <picture>
                                        <?php the_post_thumbnail('testimonials_img', array('class' => 'img-responsive')); ?>
                                    </picture>
                                    <div class="clearfix"></div>
                                    <header class="col-md-8 col-md-offset-2">
                                        <p><?php the_content(); ?></p>
                                        <h4><?php the_title(); ?></h4>
                                    </header>
                                    <div class="clearfix"></div>
                                </article>
                                <?php endwhile; ?>
                                <?php endif; ?>
                                <?php wp_reset_query(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </section>

        <section class="the-contact col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="contact-content col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <h2>Contact Us</h2>
                <h5>Harley Street Healthcare Clinic</h5>
                <h6>104 Harley Street, London, W1G 7JD</h6>


                <p>T: <a href="tel:+4402079356554" title="<?php _e('Call us', 'harleystreet'); ?>">020 7935 6554</a></p>
                <p>F: <a href="tel:+4402079356554" title="<?php _e('Call us', 'harleystreet'); ?>">020 7935 6554</a></p>
                <p>E: <a href="mailto:info@harleystreet104.com" title="<?php _e('Send us an e-mail', 'harleystreet'); ?>">info@harleystreet104.com</a></p>
                <p>W: www.harleystreet104.com</p>
                <p>Out of Hours: 020 7935 6554</p>
            </div>
            <div class="contact-map col-lg-9 col-md-9 col-sm-9 col-xs-12 no-paddingl no-paddingr">
                <?php get_template_part('templates/map'); ?>
            </div>
        </section>


    </div>
</main>
<?php get_footer(); ?>
