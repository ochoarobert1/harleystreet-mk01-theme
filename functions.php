<?php
/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_functions_overrides.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

function harleystreet_load_css() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {

            /*- BOOTSTRAP CORE ON LOCAL -*/
            wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.3.7', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME ON LOCAL -*/
            wp_register_style('bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.7', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- ANIMATE CSS ON LOCAL -*/
            wp_register_style('animate-css', get_template_directory_uri() . '/css/animate.css', false, '3.5.1', 'all');
            wp_enqueue_style('animate-css');

            /*- FONT AWESOME ON LOCAL -*/
            wp_register_style('fontawesome-css', get_template_directory_uri() . '/css/font-awesome.min.css', false, '4.6.3', 'all');
            wp_enqueue_style('fontawesome-css');

            /*- FLICKITY ON LOCAL -*/
            //wp_register_style('flickity-css', get_template_directory_uri() . '/css/flickity.min.css', false, '2.0.2', 'all');
            //wp_enqueue_style('flickity-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owl-css', get_template_directory_uri() . '/css/owl.carousel.css', false, '2.2.0', 'all');
            wp_enqueue_style('owl-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owltheme-css', get_template_directory_uri() . '/css/owl.theme.css', false, '2.2.0', 'all');
            wp_enqueue_style('owltheme-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owltrans-css', get_template_directory_uri() . '/css/owl.transitions.css', false, '2.2.0', 'all');
            wp_enqueue_style('owltrans-css');

        } else {

            /*- BOOTSTRAP CORE -*/
            wp_register_style('bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, '3.3.7', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME -*/
            wp_register_style('bootstrap-theme', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.7', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- ANIMATE CSS -*/
            wp_register_style('animate-css', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css', false, '3.5.1', 'all');
            wp_enqueue_style('animate-css');

            /*- FONT AWESOME -*/
            wp_register_style('fontawesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', false, '4.6.3', 'all');
            wp_enqueue_style('fontawesome-css');

            /*- FLICKITY -*/
            //wp_register_style('flickity-css', 'https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css', false, '2.0.2', 'all');
            //wp_enqueue_style('flickity-css');

            /*- OWL -*/
            wp_register_style('owl-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.carousel.min.css', false, '2.2.0', 'all');
            wp_enqueue_style('owl-css');

            /*- OWL -*/
            wp_register_style('owltheme-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/assets/owl.theme.default.min.css', false, '2.2.0', 'all');
            wp_enqueue_style('owltheme-css');


        }

        /*- MAIN STYLE -*/
        wp_register_style('jquery-ui-css', get_template_directory_uri() . '/css/jquery-ui.min.css', false, $version_remove, 'all');
        wp_enqueue_style('jquery-ui-css'); 

        /*- MAIN STYLE -*/
        wp_register_style('jquery-ui-time', get_template_directory_uri() . '/css/jquery-ui-timepicker-addon.css', false, $version_remove, 'all');
        wp_enqueue_style('jquery-ui-time'); 

        /*- GOOGLE FONTS -*/
        wp_register_style('google-fonts', 'https://fonts.googleapis.com/css?family=Bitter:400,400i,700|Poppins:300,400,500,600,700', false, $version_remove, 'all');
        wp_enqueue_style('google-fonts');

        /*- MAIN STYLE -*/
        wp_register_style('main-style', get_template_directory_uri() . '/css/harleystreet-style.css', false, $version_remove, 'all');
        wp_enqueue_style('main-style');

        /*- MAIN MEDIAQUERIES -*/
        wp_register_style('main-mediaqueries', get_template_directory_uri() . '/css/harleystreet-mediaqueries.css', array('main-style'), $version_remove, 'all');
        wp_enqueue_style('main-mediaqueries');

        /*- WORDPRESS STYLE -*/
        wp_register_style('wp-initial-style', get_template_directory_uri() . '/style.css', false, $version_remove, 'all');
        wp_enqueue_style('wp-initial-style');
    }
}

add_action('init', 'harleystreet_load_css');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", '');
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '1.12.4', false);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', false, '1.12.4', false);
    }
    wp_enqueue_script('jquery');
}


function harleystreet_load_js() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            /*- MODERNIZR ON LOCAL  -*/
            wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP ON LOCAL  -*/
            wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY ON LOCAL  -*/
            wp_register_script('sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '1.0.4', true);
            wp_enqueue_script('sticky');

            /*- JQUERY NICESCROLL ON LOCAL  -*/
            wp_register_script('nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array('jquery'), '3.6.8', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            //wp_register_script('lettering', get_template_directory_uri() . '/js/jquery.lettering.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- SMOOTH SCROLL -*/
            //wp_register_script('smooth-scroll', get_template_directory_uri() . '/js/smooth-scroll.js', array('jquery'), '10.0.0', true);
            //wp_enqueue_script('smooth-scroll');

            /*- IMAGESLOADED ON LOCAL  -*/
            //wp_register_script('imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('imagesloaded');

            /*- ISOTOPE ON LOCAL  -*/
            //wp_register_script('isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), '3.0.1', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY ON LOCAL  -*/
            //wp_register_script('flickity', get_template_directory_uri() . '/js/flickity.pkgd.min.js', array('jquery'), '2.0.2', true);
            //wp_enqueue_script('flickity');

            /*- MASONRY ON LOCAL  -*/
            //wp_register_script('masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('masonry');

            /*- OWL ON LOCAL -*/
            wp_register_script('owl-js', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '2.2.0', true);
            wp_enqueue_script('owl-js');

        } else {


            /*- MODERNIZR -*/
            wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP -*/
            wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY -*/
            wp_register_script('sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.min.js', array('jquery'), '1.0.3', true);
            wp_enqueue_script('sticky');

            /*- JQUERY NICESCROLL -*/
            wp_register_script('nicescroll', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js', array('jquery'), '3.6.8', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            //wp_register_script('lettering', 'https://cdnjs.cloudflare.com/ajax/libs/lettering.js/0.7.0/jquery.lettering.min.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- SMOOTH SCROLL -*/
            //wp_register_script('smooth-scroll', 'https://cdnjs.cloudflare.com/ajax/libs/smooth-scroll/10.0.0/js/smooth-scroll.min.js', array('jquery'), '10.0.0', true);
            //wp_enqueue_script('smooth-scroll');

            /*- IMAGESLOADED -*/
            //wp_register_script('imagesloaded', 'https://cdn.jsdelivr.net/imagesloaded/4.1.0/imagesloaded.pkgd.min.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('imagesloaded');

            /*- ISOTOPE -*/
            //wp_register_script('isotope', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.1/isotope.pkgd.min.js', array('jquery'), '3.0.1', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY -*/
            //wp_register_script('flickity', 'https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.pkgd.min.js', array('jquery'), '1.2.1', true);
            //wp_enqueue_script('flickity');

            /*- MASONRY -*/
            //wp_register_script('masonry', 'https://cdnjs.cloudflare.com/ajax/libs/masonry/4.1.0/masonry.pkgd.min.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('masonry');

            /*- OWL ON LOCAL -*/
            wp_register_script('owl-js', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.0/owl.carousel.min.js', array('jquery'), '2.2.0', true);
            wp_enqueue_script('owl-js');

        }

        /*- MAIN FUNCTIONS -*/
        wp_register_script('jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('jquery-ui');

        /*- MAIN FUNCTIONS -*/
        wp_register_script('jquery-time', get_template_directory_uri() . '/js/jquery-ui-timepicker-addon.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('jquery-time');

        /*- MAIN FUNCTIONS -*/
        wp_register_script('main-functions', get_template_directory_uri() . '/js/functions.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('main-functions');
    }
}

add_action('init', 'harleystreet_load_js');

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain( 'harleystreet', get_template_directory() . '/languages' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ));
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'menus' );
add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form' ) );
add_theme_support( 'custom-background',
                  array(
    'default-image' => '',    // background image default
    'default-color' => '',    // background color default (dont add the #)
    'wp-head-callback' => '_custom_background_cb',
    'admin-head-callback' => '',
    'admin-preview-callback' => ''
)
                 );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */
function harleystreet_add_editor_styles() {
    add_editor_style( get_stylesheet_directory_uri() . '/css/editor-styles.css' );
}
add_action( 'admin_init', 'harleystreet_add_editor_styles' );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'header_menu' => __( 'Menu Header Principal', 'harleystreet' ),
    'footer_menu' => __( 'Menu Footer', 'harleystreet' ),
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'harleystreet_widgets_init' );
function harleystreet_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'harleystreet' ),
        'id' => 'main_sidebar',
        'description' => __( 'Widgets seran vistos en posts y pages', 'harleystreet' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Shop Sidebar', 'harleystreet' ),
        'id' => 'shop_sidebar',
        'description' => __( 'Widgets seran vistos en Tienda y Categorias de Producto', 'harleystreet' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

add_action('login_head', 'custom_login_logo');
function custom_login_logo() {
    echo '
    <style type="text/css">
        body{
            background-color: #f5f5f5 !important;
            background-image:url(' . get_template_directory_uri() . '/images/bg.jpg);
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
        h1 {
            background-image:url(' . get_template_directory_uri() . '/images/logo.png) !important;
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain !important;
        }
        a { background-image:none !important; }
        .login form{
            -webkit-border-radius: 5px;
            border-radius: 5px;
            background-color: rgba(203, 203, 203, 0.5);
            box-shadow: 0px 0px 6px #878787;
        }
        .login label{
            color: black;
            font-weight: 500;
        }
    </style>
    ';
}

if (! function_exists('dashboard_footer') ){
    function dashboard_footer() {
        echo '<span id="footer-thankyou">';
        _e ('Gracias por crear con ', 'harleystreet' );
        echo '<a href="http://wordpress.org/" >WordPress.</a> - ';
        _e ('Tema desarrollado por ', 'harleystreet' );
        echo '<a href="http://robertochoa.com.ve/" >Robert Ochoa</a></span>';
    }
}
add_filter('admin_footer_text', 'dashboard_footer');


/* --------------------------------------------------------------
    GET CUSTOM REVSLIDER
-------------------------------------------------------------- */

function get_custom_slider() {
    global $wpdb;
    $the_query = "SELECT title, alias FROM " . $wpdb->prefix . "revslider_sliders";
    $sliders = $wpdb->get_results($the_query, 'ARRAY_A');
    $item_container = [];
    $i = 1;
    if (empty($sliders)) {
        $item_container[] = array(" " => " ");
    } else {
        foreach ($sliders as $item){
            $itemkeys[] = $item['alias'];
            $itemvalues[] = $item['title'];
        }
        $item_container[] = array_combine($itemkeys, $itemvalues);
    }
    return $item_container[0];
}

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

add_filter( 'rwmb_meta_boxes', 'harleystreet_metabox' );

function harleystreet_metabox( $meta_boxes )
{
    $prefix = 'rw_';

    $meta_boxes[] = array(
        'id'         => 'home_data',
        'title'      => __( 'Extra Information', 'harleystreet' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'include' => array(
        // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
        'relation'        => 'OR',
        'slug'            => array( 'home' ),
        'template'        => array( 'page-home.php' ),
    ),
        'fields' => array(
        array(
        'name'     => __( 'Select Revolution Slider', 'harleystreet' ),
        'id'       => $prefix . 'slider',
        'type'     => 'select_advanced',
        // Array of 'value' => 'Label' pairs for select box
        'options'  => get_custom_slider(),
        // Select multiple values, optional. Default is false.
        'multiple' => false,
        'std'         => ' ', // Default value, optional
        'placeholder' => __( 'Select an slider', 'harleystreet' ),
    ),
    )
    );

    $meta_boxes[] = array(
        'id'         => 'service_data',
        'title'      => __( 'Extra Information', 'harleystreet' ),
        'post_types' => array( 'services' ),
        'context'    => 'side',
        'priority'   => 'high',
        'fields' => array(
        array(
        'name'  => __( 'Service Logo', 'harleysteet' ),
        'desc'  => __( 'Recommended Format: PNG 50x50px', 'harleysteet' ),
        'id'    => $prefix . 'service_logo',
        'type'  => 'image_advanced',
    ),
        array(
        'name'  => __( 'Service Price (Optional)', 'harleysteet' ),
        'desc'  => __( 'Price from Service', 'harleysteet' ),
        'id'    => $prefix . 'service_price',
        'type'  => 'text',
    ),
    )
    );

    $meta_boxes[] = array(
        'id'         => 'page_data',
        'title'      => __( 'Extra Information', 'harleystreet' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
        array(
        'name'  => __( 'Banner Page', 'harleysteet' ),
        'desc'  => __( 'Recommended Format: JPG 1350x500px', 'harleysteet' ),
        'id'    => $prefix . 'page_banner',
        'type'  => 'image_advanced',
    ),
    )
    );

    return $meta_boxes;
}

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

// Register Custom Post Type
function services() {

    $labels = array(
        'name'                  => _x( 'Services', 'Post Type General Name', 'harleystreet' ),
        'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'harleystreet' ),
        'menu_name'             => __( 'Services', 'harleystreet' ),
        'name_admin_bar'        => __( 'Services', 'harleystreet' ),
        'archives'              => __( 'Services Archives', 'harleystreet' ),
        'attributes'            => __( 'Services Attributes', 'harleystreet' ),
        'parent_item_colon'     => __( 'Parent Service:', 'harleystreet' ),
        'all_items'             => __( 'All Services', 'harleystreet' ),
        'add_new_item'          => __( 'Add New Service', 'harleystreet' ),
        'add_new'               => __( 'Add New', 'harleystreet' ),
        'new_item'              => __( 'New Service', 'harleystreet' ),
        'edit_item'             => __( 'Edit Service', 'harleystreet' ),
        'update_item'           => __( 'Update Service', 'harleystreet' ),
        'view_item'             => __( 'View Service', 'harleystreet' ),
        'view_items'            => __( 'View Services', 'harleystreet' ),
        'search_items'          => __( 'Search Service', 'harleystreet' ),
        'not_found'             => __( 'Not found', 'harleystreet' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'harleystreet' ),
        'featured_image'        => __( 'Featured Image', 'harleystreet' ),
        'set_featured_image'    => __( 'Set featured image', 'harleystreet' ),
        'remove_featured_image' => __( 'Remove featured image', 'harleystreet' ),
        'use_featured_image'    => __( 'Use as featured image', 'harleystreet' ),
        'insert_into_item'      => __( 'Insert into Service', 'harleystreet' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Service', 'harleystreet' ),
        'items_list'            => __( 'Services list', 'harleystreet' ),
        'items_list_navigation' => __( 'Services list navigation', 'harleystreet' ),
        'filter_items_list'     => __( 'Filter Services list', 'harleystreet' ),
    );
    $args = array(
        'label'                 => __( 'Service', 'harleystreet' ),
        'description'           => __( 'Services of organization', 'harleystreet' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes' ),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 15,
        'menu_icon'             => 'dashicons-megaphone',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,		
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'show_in_rest'          => false,
    );
    register_post_type( 'services', $args );

}
add_action( 'init', 'services', 0 );

function partners() {

    $labels = array(
        'name'                  => _x( 'Partners', 'Post Type General Name', 'harleystreet' ),
        'singular_name'         => _x( 'Partner', 'Post Type Singular Name', 'harleystreet' ),
        'menu_name'             => __( 'Partners', 'harleystreet' ),
        'name_admin_bar'        => __( 'Partners', 'harleystreet' ),
        'archives'              => __( 'Partner Archives', 'harleystreet' ),
        'attributes'            => __( 'Partner Attributes', 'harleystreet' ),
        'parent_item_colon'     => __( 'Parent Partner:', 'harleystreet' ),
        'all_items'             => __( 'All Partners', 'harleystreet' ),
        'add_new_item'          => __( 'Add New Partner', 'harleystreet' ),
        'add_new'               => __( 'Add New', 'harleystreet' ),
        'new_item'              => __( 'New Partner', 'harleystreet' ),
        'edit_item'             => __( 'Edit Partner', 'harleystreet' ),
        'update_item'           => __( 'Update Partner', 'harleystreet' ),
        'view_item'             => __( 'View Partner', 'harleystreet' ),
        'view_items'            => __( 'View Partners', 'harleystreet' ),
        'search_items'          => __( 'Search Partner', 'harleystreet' ),
        'not_found'             => __( 'Not found', 'harleystreet' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'harleystreet' ),
        'featured_image'        => __( 'Featured Image', 'harleystreet' ),
        'set_featured_image'    => __( 'Set featured image', 'harleystreet' ),
        'remove_featured_image' => __( 'Remove featured image', 'harleystreet' ),
        'use_featured_image'    => __( 'Use as featured image', 'harleystreet' ),
        'insert_into_item'      => __( 'Insert into Partner', 'harleystreet' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Partner', 'harleystreet' ),
        'items_list'            => __( 'Partners list', 'harleystreet' ),
        'items_list_navigation' => __( 'Partners list navigation', 'harleystreet' ),
        'filter_items_list'     => __( 'Filter Partners list', 'harleystreet' ),
    );
    $args = array(
        'label'                 => __( 'Partner', 'harleystreet' ),
        'description'           => __( 'Partners of Organization', 'harleystreet' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 17,
        'menu_icon'             => 'dashicons-id-alt',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,		
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'show_in_rest'          => false,
    );
    register_post_type( 'partners', $args );

}
add_action( 'init', 'partners', 0 );

function testimonials() {

    $labels = array(
        'name'                  => _x( 'Testimonials', 'Post Type General Name', 'harleystreet' ),
        'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'harleystreet' ),
        'menu_name'             => __( 'Testimonials', 'harleystreet' ),
        'name_admin_bar'        => __( 'Testimonials', 'harleystreet' ),
        'archives'              => __( 'Testimonial Archives', 'harleystreet' ),
        'attributes'            => __( 'Testimonial Attributes', 'harleystreet' ),
        'parent_item_colon'     => __( 'Parent Testimonial:', 'harleystreet' ),
        'all_items'             => __( 'All Testimonials', 'harleystreet' ),
        'add_new_item'          => __( 'Add New Testimonial', 'harleystreet' ),
        'add_new'               => __( 'Add New', 'harleystreet' ),
        'new_item'              => __( 'New Testimonial', 'harleystreet' ),
        'edit_item'             => __( 'Edit Testimonial', 'harleystreet' ),
        'update_item'           => __( 'Update Testimonial', 'harleystreet' ),
        'view_item'             => __( 'View Testimonial', 'harleystreet' ),
        'view_items'            => __( 'View Testimonials', 'harleystreet' ),
        'search_items'          => __( 'Search Testimonial', 'harleystreet' ),
        'not_found'             => __( 'Not found', 'harleystreet' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'harleystreet' ),
        'featured_image'        => __( 'Featured Image', 'harleystreet' ),
        'set_featured_image'    => __( 'Set featured image', 'harleystreet' ),
        'remove_featured_image' => __( 'Remove featured image', 'harleystreet' ),
        'use_featured_image'    => __( 'Use as featured image', 'harleystreet' ),
        'insert_into_item'      => __( 'Insert into Testimonial', 'harleystreet' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'harleystreet' ),
        'items_list'            => __( 'Testimonials list', 'harleystreet' ),
        'items_list_navigation' => __( 'Testimonials list navigation', 'harleystreet' ),
        'filter_items_list'     => __( 'Filter Testimonials list', 'harleystreet' ),
    );
    $args = array(
        'label'                 => __( 'Testimonial', 'harleystreet' ),
        'description'           => __( 'Testimonials from Clients', 'harleystreet' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 18,
        'menu_icon'             => 'dashicons-cloud',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,		
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'show_in_rest'          => false,
    );
    register_post_type( 'testimonials', $args );

}
add_action( 'init', 'testimonials', 0 );

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists( 'add_image_size' ) ) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('blog_img', 276, 217, true);
    add_image_size( 'product_img', 330, 220, true );
    add_image_size( 'single_img', 636, 297, true );
    add_image_size( 'testimonials_img', 115, 115, true );

}


/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

// WALKER COMPLETO TOMADO DESDE EL NAVBAR COLLAPSE
//require_once('includes/wp_bootstrap_navwalker.php');

// WALKER CUSTOM SI DEBO COLOCAR ICONOS AL LADO DEL MENU PRINCIPAL - SU ESTRUCTURA ESTA DENTRO DEL MISMO ARCHIVO
require_once('includes/wp_walker_custom.php');

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */

require_once('includes/wp_woocommerce_functions.php');

add_action( 'pre_get_posts', 'my_change_sort_order'); 
function my_change_sort_order($query){
    if (!is_admin()) {
        if ( is_main_query() ) {
            if(is_post_type_archive('services')):
            //If you wanted it for the archive of a custom post type use: is_post_type_archive( $post_type )
            $query->set( 'posts_per_page', -1 );

            endif;    
        }
    }
};

// Add Shortcode
function custom_accordion( $atts, $content = null ) {
    $new_url = sanitize_title(esc_attr($atts['title']));
?>

<a class="custom-collapse collapsed" data-toggle="collapse" href="#collapse-<?php echo $new_url; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $new_url; ?>">
    <?php echo esc_attr($atts['order']); ?>. <?php echo esc_attr($atts['title']); ?>
    <?php if ($content == "") { echo ''; } else { ?>
    <span class="custom-collapse-caret">+</span>
    <?php } ?>
</a>
<div class="collapse" id="collapse-<?php echo $new_url; ?>">
    <div class="custom-collapse-container">
        <?php echo $content; ?>
    </div>
</div>

<?php } add_shortcode( 'accordion', 'custom_accordion' );

add_action('admin_head', 'harleystreet_add_my_tc_button');

function harleystreet_add_my_tc_button() {
    global $typenow;
    // check user permissions
    if ( !current_user_can('edit_posts') && !current_user_can('edit_pages') ) {
        return;
    }
    // check if WYSIWYG is enabled
    if ( get_user_option('rich_editing') == 'true') {
        add_filter("mce_external_plugins", "gavickpro_add_tinymce_plugin");
        add_filter('mce_buttons', 'gavickpro_register_my_tc_button');
    }
}

function gavickpro_add_tinymce_plugin($plugin_array) {
    $plugin_array['gavickpro_tc_button'] = get_template_directory_uri() . '/js/text-button.js'; // CHANGE THE BUTTON SCRIPT HERE
    return $plugin_array;
}

function gavickpro_register_my_tc_button($buttons) {
    array_push($buttons, "gavickpro_tc_button");
    return $buttons;
}



/* SERVICE FUNCTION CALLER */
add_action( 'wp_enqueue_scripts', 'ajax_test_enqueue_scripts' );

function ajax_test_enqueue_scripts() {

    if ( is_page( 'book-an-appointment') ) {

        wp_enqueue_script( 'servfunctions', get_template_directory_uri() . '/js/serv-functions.js', array('jquery'), '1.0', true );

        wp_localize_script( 'servfunctions', 'servfunctions', array(
            'ajax_url' => admin_url( 'admin-ajax.php' )
        ));

    }

}

add_action( 'wp_ajax_nopriv_post_servfunctions', 'post_servfunctions' );
add_action( 'wp_ajax_post_servfunctions', 'post_servfunctions' );

function post_servfunctions() {

    $arrayserv = array();
    $services = get_posts( array( 'post_type' => 'services', 'posts_per_page' => -1, 'post_parent' => 0 ) );
    $i = 1;
    foreach ($services as $items) {
        $arrayserv[$i] = $items->ID . ', ' . $items->post_title;
        $i++;
    }
    echo json_encode($arrayserv);

    die();

}

add_filter( 'posts_orderby', 'order_search_by_posttype', 10, 1 );
function order_search_by_posttype( $orderby ){
    if( ! is_admin() && is_search() ) :
    global $wpdb;
    $orderby =
        "
            CASE WHEN {$wpdb->prefix}posts.post_type = 'services' THEN '1' 
                 WHEN {$wpdb->prefix}posts.post_type = 'post' THEN '2' 
                 WHEN {$wpdb->prefix}posts.post_type = 'page' THEN '3' 
                 WHEN {$wpdb->prefix}posts.post_type = 'product' THEN '4' 
            ELSE {$wpdb->prefix}posts.post_type END ASC, 
            {$wpdb->prefix}posts.post_title ASC";
    endif;
    return $orderby;
}




?>
