<?php get_header(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-title-container service-banner-section col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="page-title-content  col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <h1 itemprop="headline"><?php _e('OUR SERVICES','harleystreet'); ?></h1>
                        <div class="clearfix"></div>
                        <div class=" col-lg-7 col-md-7 col-sm-7 col-xs-12 no-paddingl no-paddingr">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias rem velit cum suscipit nulla explicabo expedita architecto excepturi esse ut maxime quidem quo, minima perferendis optio. Sed, dolore odio rem.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="container">
                <div class="row">
                    <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php $defaultatts = array('class' => 'img-responsive'); ?>
                        <?php if (have_posts()): ?>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php $i = 1; ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <?php if ( $post->post_parent > 0 ) { ?>

                            <?php } else { ?>
                            <div class="panel panel-default panel-custom <?php echo join(' ', get_post_class()); ?>" role="article">
                                <div class="panel-heading" role="tab" id="heading-<?php echo get_the_ID(); ?>">
                                    <h4 class="panel-title <?php if ($i > 1) { echo 'collapsed'; } ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo get_the_ID(); ?>" aria-expanded="true" aria-controls="collapse-<?php echo get_the_ID(); ?>">
                                        <a>
                                            <div class="panel-title-img">
                                                <?php $images = rwmb_meta( 'rw_service_logo', 'size=avatar' ); ?>
                                                <?php if ( !empty( $images ) ) { foreach ( $images as $image ) { ?>
                                                <?php echo "<img src='{$image['url']}' class='img-responsive' width='{$image['width']}' height='{$image['height']}' alt='{$image['alt']}' />"; ?>
                                                <?php } } ?>
                                            </div> 
                                            <?php the_title(); ?> 
                                            <div class="panel-title-chevron"><i class="fa fa-chevron-up"></i></div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse-<?php echo get_the_ID(); ?>" class="panel-collapse collapse <?php if ($i == 1) { echo 'in'; } ?>" role="tabpanel" aria-labelledby="heading-<?php echo get_the_ID(); ?>">
                                    <div class="panel-body">
                                        <div class="panel-body-sharer">
                                            <a onclick="share_toggle(<?php echo get_the_ID(); ?>);" class="share-toggle"><span><?php _e('Share this service', 'harleystreet'); ?></span> <i class="fa fa-share-alt"></i></a>
                                            <div id="sharebtn_<?php echo get_the_ID(); ?>" class="share-buttons-container share-button-hide">
                                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_the_permalink())?>" class="share-link" target="_
                                                "><i class="fa fa-facebook"></i></a>
                                                <a href="http://twitter.com/intent/tweet?status=<?php echo urlencode(get_the_title()); ?>+<?php echo urlencode(get_the_permalink())?>" target="_
                                                " class="share-link"><i class="fa fa-twitter"></i></a>
                                                <a href="mailto:wordpress@domain.com?&subject=<?php echo urlencode(get_the_title()); ?>&body=<?php echo urlencode(get_the_permalink())?>" class="share-link"><i class="fa fa-envelope"></i></a>
                                            </div>
                                        </div>
                                        <?php if (has_post_thumbnail() ) { ?>
                                        <div class="panel-content-services-img col-md-6 no-paddingl">
                                            <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                                        </div>
                                        <div class="panel-content-services col-md-6 no-paddingr">
                                            <?php the_content(); ?>    
                                        </div>
                                        <?php } else { ?>
                                        <div class="panel-content-services col-md-12 no-paddingl no-paddingr">
                                            <?php the_content(); ?>
                                        </div>
                                        <?php } ?>

                                        <div class="clearfix"></div>

                                        <?php $postmain = get_the_ID(); ?>
                                        <?php $my_wp_query = new WP_Query(); ?>
                                        <?php $all_wp_pages = $my_wp_query->query(array('post_type' => 'services', 'orderby'=>'date', 'order' => 'ASC')); ?>
                                        <?php $portfolio_children = get_page_children( get_the_ID(), $all_wp_pages ); ?>
                                        <?php $y = 1; ?>
                                        <?php if (!empty($portfolio_children)) { ?>
                                        <div class="service-child-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                            <?php foreach ($portfolio_children as $item) { ?>
                                            <div class="service-child-item col-md-6">
                                                <div class="service-child-item-title col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                                    <h3>
                                                        <?php echo $item->post_title; ?> 
                                                        <?php $price_serv = get_post_meta($item->ID, 'rw_service_price', true); ?>
                                                        <?php if ($price_serv != '') { ?>
                                                        <div class="pull-right">
                                                            $ <?php echo $price_serv; ?>
                                                        </div>
                                                        <?php } ?>
                                                    </h3>
                                                </div>
                                                <div class="service-child-item-content col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                                    <?php echo apply_filters('the_content', $item->post_content); ?>
                                                </div>
                                            </div>
                                            <?php $y++; if ($y > 2) { $y = 1; echo '<div class="clearfix"></div>'; } } ?>
                                        </div>
                                        <?php } wp_reset_postdata(); ?>
                                        <div class="clearfix"></div>
                                        <div class="panel-body-button">
                                            <a href="https://www.astutedatasystems.com/home.aspx?ID=780" title="<?php _e('Book An Appointment', 'harleystreet'); ?>"><button class="btn btn-md btn-services"><?php _e('Book An Appointment', 'harleystreet'); ?></button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php $i++; endwhile; ?>
                        </div>
                        <?php else: ?>
                        <article>
                            <h2>Disculpe, su busqueda no arrojo ningun resultado</h2>
                            <h3>Haga click <a href="<?php echo home_url('/'); ?>">aqui</a> para volver al inicio</h3>
                        </article>
                        <?php endif; ?>
                    </section>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
