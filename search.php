<?php get_header(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <section class="main-banner-section shop-banner-section col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <h1 class="page-title"><?php echo sprintf( __( '%s Search Results for ', 'harleystreet' ), $wp_query->found_posts ); echo esc_attr(get_search_query()); ?></h1>
                        <?php echo get_search_form(); ?>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error placeat sit odio possimus ratione non facere, impedit quia omnis incidunt eum nobis iusto, temporibus nihil reiciendis perspiciatis assumenda inventore autem voluptatum.</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php $defaultatts = array('class' => 'img-responsive'); ?>
                        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" class="archive-item search-item col-lg-6 col-md-6 col-sm-6 col-xs-12 <?php echo join(' ', get_post_class()); ?>" role="article">
                            <div class="search-item-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><h2><?php the_title(); ?></h2></a>
                                <p><?php the_excerpt(); ?></p>
                            </div>
                            <div class="clearfix"></div>
                        </article>
                        <?php endwhile; ?>
                        <div class="pagination col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); } ?>
                        </div>
                    </div>
                    <?php else: ?>
                    <article class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2>Disculpe, su busqueda no arrojo ningun resultado</h2>
                        <h3>Haga click <a href="<?php echo home_url('/'); ?>">aqui</a> para volver al inicio</h3>
                    </article>
                    <?php endif; ?>
                </div>
            </div>


        </section>
    </div>
</main>
<?php get_footer(); ?>


