<?php
/* WOOCOMMERCE CUSTOM COMMANDS */

/* WOOCOMMERCE - DECLARE THEME SUPPORT - BEGIN */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
/* WOOCOMMERCE - DECLARE THEME SUPPORT - END */

/* WOOCOMMERCE - CUSTOM WRAPPER - BEGIN */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
    echo '<section id="main" class="container"><div class="row"><div class="woocustom-main-container col-lg-12 col-md-12 col-sm-12 col-xs-12">';
}

function my_theme_wrapper_end() {
    echo '</div></div></section>';
}
/* WOOCOMMERCE - CUSTOM WRAPPER - END */


/* WOOCOMMERCE - REMOVE RESULT COUNT - BEGIN  */

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20);

/* WOOCOMMERCE - REMOVE RESULT COUNT - END  */

/* WOOCOMMERCE - REMOVE CATALOG ORDERING - BEGIN  */

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

/* WOOCOMMERCE - REMOVE CATALOG ORDERING - END  */


/* WOOCOMMERCE - CHANGE ADD TO CART BUTTON TEXTS - BEGIN  */

add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +

function woo_custom_cart_button_text() {
    return __( 'Buy Now', 'harleystreet' );
}

add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );    // 2.1 +

function woo_archive_custom_cart_button_text() {
    return __( 'Add to Cart', 'harleystreet' );
}

/* WOOCOMMERCE - CHANGE ADD TO CART BUTTON TEXTS - END  */


/* WOOCOMMERCE - REMOVE SINGLE PRODUCT TABS - BEGIN  */

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);

/* WOOCOMMERCE - REMOVE SINGLE PRODUCT TABS - END  */

// define the woocommerce_share callback
function action_woocommerce_share( ) {

    echo '<div id="sharebtn_product" class="woocommerce-custom-sharer"><a>';
    _e('Share this product', 'harleystreet');
    echo ' <i class="fa fa-share-alt"></i></a></div>';
    echo '<div id="share_product" class="share-buttons-container share-buttons-product share-button-hide">';
    echo '<a href="https://www.facebook.com/sharer/sharer.php?u=' . urlencode(get_the_permalink()) . '" class="share-link" target="_blank"><i class="fa fa-facebook"></i></a>';
    echo '<a href="http://twitter.com/intent/tweet?status=' . urlencode(get_the_title()) . ' + ' . urlencode(get_the_permalink()) .'" target="_blank" class="share-link"><i class="fa fa-twitter"></i></a>';
    echo '<a href="mailto:wordpress@domain.com?&subject=' . urlencode(get_the_title()) . '&body=' . urlencode(get_the_permalink()) .'" class="share-link"><i class="fa fa-envelope"></i></a>';
    echo '</div>';

};

// add the action
add_action( 'woocommerce_share', 'action_woocommerce_share', 10, 2 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);


remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 6 );





?>
