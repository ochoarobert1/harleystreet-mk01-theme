<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-title-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="page-title-content col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <?php $title = $post_slug=$post->post_name; ?>
                        <h1 itemprop="headline"><?php if ($title == 'cart') { echo '<i class="fa fa-shopping-cart"></i>'; }?>  <?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="container">
                <div class="row">
                    <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
                        <div class="page-article col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" itemprop="articleBody">
                            <?php the_content(); ?>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
